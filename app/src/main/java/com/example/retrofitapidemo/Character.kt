package com.example.retrofitapidemo

import com.google.gson.annotations.SerializedName

class Character(@SerializedName("name") val fullName: String, val image: String)