package com.example.retrofitapidemo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private val api by lazy { createRickAndMortyApi() }
    private val adapter by lazy { CharacterAdapter() }
    private val repository by lazy { Repository(api) }
    private val viewModel by lazy { MainViewModel(repository) }

    companion object {
        private const val BASE_URL = "https://rickandmortyapi.com/api/"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecyclerView()
        observeCharactersChanges()
        observerErrorChanges()
        viewModel.onViewCreated()
    }

    private fun observeCharactersChanges() {
        viewModel.characters.observe(this) {
            showCharacters(it)
        }
    }

    private fun observerErrorChanges() {
        viewModel.error.observe(this) { throwable ->
            throwable.message?.let { showErrorMessage(it) }
        }
    }

    private fun showCharacters(characters: List<Character>) {
        adapter.setCharacters(characters)
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = adapter
    }

    private fun showErrorMessage(message: String) {
        Toast.makeText(
            this,
            "An error occured code: $message",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun createRickAndMortyApi(): RickAndMortyApi {
        val interceptor =
            HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
        return retrofit.create(RickAndMortyApi::class.java)
    }
}
