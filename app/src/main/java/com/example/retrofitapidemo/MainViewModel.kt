package com.example.retrofitapidemo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(private val repository: Repository) : ViewModel() {
    internal val characters = MutableLiveData<List<Character>>()
    internal val error = MutableLiveData<Throwable>()

    fun onViewCreated() {
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                kotlin.runCatching {
                    repository.getCharacters()
                }
            }
            result.onSuccess { characters.value = it.results }
            result.onFailure { error.value = it }
        }
    }

}