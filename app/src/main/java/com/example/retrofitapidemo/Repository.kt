package com.example.retrofitapidemo

class Repository(private val api: RickAndMortyApi) {
    suspend fun getCharacters() = api.getCharacters()
}